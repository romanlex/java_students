package students.controllers;

import students.models.dao.StudentDAOImpl;
import students.models.pojo.Student;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StudentsListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArrayList<Student> studentlist = new ArrayList<>();
        StudentDAOImpl students = new StudentDAOImpl();
        try (ResultSet rs = students.find()) {
            int rows = 0;

            if (rs.last()) {
                rows = rs.getRow();
                rs.beforeFirst();
            }


            int i = 0;
            while (rs.next()) {
                i++;
                Student student = new Student();
                student.setId(rs.getInt("id"));
                student.setName(rs.getString("name"));
                student.setGroup_id(rs.getInt("group_id"));
                student.setBirthday(rs.getString("birthday"));
                student.setSex(rs.getString("sex"));
                studentlist.add(student);
            }

            req.setAttribute("students", studentlist);
            req.getRequestDispatcher("/students_list.jsp").forward(req, resp);
        } catch (SQLException e) {
            req.setAttribute("error", e.getMessage());
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
            dispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
