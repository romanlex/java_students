package students.controllers;

import students.models.dao.LectureDAO;
import students.models.pojo.Lecture;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class LecturesListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArrayList<Lecture> lecturesList = new ArrayList<>();
        LectureDAO lectures = new LectureDAO();
        try (ResultSet rs = lectures.find()) {
            int rows = 0;

            if (rs.last()) {
                rows = rs.getRow();
                rs.beforeFirst();
            }


            int i = 0;
            while (rs.next()) {
                i++;
                Lecture lecture = new Lecture();
                lecture.setId(rs.getInt("id"));
                lecture.setTitle(rs.getString("title"));
                lecture.setText(rs.getString("text"));
                lecture.setSubject(rs.getString("subject"));
                lecturesList.add(lecture);
            }

            req.setAttribute("lectures", lecturesList);
            req.getRequestDispatcher("/lectures_list.jsp").forward(req, resp);
        } catch (SQLException e) {
            req.setAttribute("error", e.getMessage());
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
            dispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
