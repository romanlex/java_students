package students.controllers;

import students.services.StudentServiceImpl;
import students.common.exceptions.StudentDAOException;
import students.exceptions.ModelException;
import students.models.dao.StudentDAOImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class StudentsAddServlet extends HttpServlet {
    private static Logger log = Logger.getLogger(StudentDAOImpl.class);

    @Autowired
    private StudentServiceImpl studentService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/add.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (req.getParameter("group_id").equals("")) {
            req.setAttribute("error", "Не указан номе группы");
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }

        if (req.getParameter("name").equals("")) {
            req.setAttribute("error", "Не указано имя");
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }

        if (req.getParameter("birthday").equals("")) {
            req.setAttribute("error", "Не указан день рождения");
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }

        if (req.getParameter("sex").equals("")) {
            req.setAttribute("error", "Не указан пол");
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }

        String groupId = req.getParameter("group_id");
        String name = req.getParameter("name");
        String birthday = req.getParameter("birthday");
        String sex = req.getParameter("sex");

        try {
            if (studentService.addStudent(groupId, name, birthday, sex)) {
                resp.sendRedirect("/students/list");
            } else {
                resp.sendRedirect("/students/add");
            }
        } catch (StudentDAOException | ModelException e) {
            log.error(e);
            req.setAttribute("error", "Cannot add student: " + e.getMessage());
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
            dispatcher.forward(req, resp);
        }
    }
}
