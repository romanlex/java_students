package students.controllers;

import students.services.LectureServiceImpl;
import students.common.exceptions.LectureDAOException;
import students.exceptions.ModelException;
import students.models.dao.StudentDAOImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class LecturesAddServlet extends ControllerBase {
    private static Logger log = Logger.getLogger(StudentDAOImpl.class);

    @Autowired
    private LectureServiceImpl lectureService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/lectures_add.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (req.getParameter("title").equals("")) {
            req.setAttribute("error", "Не указано наименование лекции");
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }

        if (req.getParameter("text").equals("")) {
            req.setAttribute("error", "Текст лекции не может быть пустой");
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }

        if (req.getParameter("subject").equals("")) {
            req.setAttribute("error", "Укажите тему лекции");
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }

        if (req.getParameter("start").equals("")) {
            req.setAttribute("error", "Укажите начало лекции");
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }


        HashMap<String, Object> data = new HashMap<>();

        for (Object key:
                req.getParameterMap().keySet()) {
            String _key = key.toString();
            String[] value = req.getParameterMap().get(key);
            data.put(_key, value[0]);
        }

        try {
            if (lectureService.saveLecture(data) > 0) {
                resp.sendRedirect("/lectures/");
            } else {
                resp.sendRedirect("/lectures/add");
            }
        } catch (ModelException | LectureDAOException e) {
            log.error(e);
            req.setAttribute("error", "Cannot add lecture: " + e.getMessage());
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
            dispatcher.forward(req, resp);
        }
    }
}
