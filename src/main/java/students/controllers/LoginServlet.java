package students.controllers;

import students.services.UserService;
import students.common.exceptions.UserDAOException;
import students.models.dao.StudentDAOImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginServlet extends ControllerBase {
    private static Logger log = Logger.getLogger(StudentDAOImpl.class);

    @Autowired
    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        try {
            if (userService.authenticated(login, password)) {
                HttpSession session = req.getSession();
                session.setAttribute("login", login);
                resp.sendRedirect("/dashboard");
            } else {
                resp.sendRedirect("/");
            }
        } catch (UserDAOException e) {
            log.error(e);
            req.setAttribute("error", e.getMessage());
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
            dispatcher.forward(req, resp);
        }
    }
}
