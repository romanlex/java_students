package students.controllers;

import students.services.LectureServiceImpl;
import students.common.exceptions.LectureDAOException;
import students.exceptions.ModelException;
import students.models.pojo.Lecture;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class LecturesEditServlet extends HttpServlet {
    private static Logger log = Logger.getLogger(LecturesEditServlet.class);

    @Autowired
    private LectureServiceImpl lectureService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req.getParameter("id") == null)
        {
            req.setAttribute("error", "ID not getted");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
            dispatcher.forward(req, resp);
        }

        int id = Integer.parseInt(req.getParameter("id"));
        try {
            Lecture lecture = lectureService.getLectureById(id);
            req.setAttribute("lecture", lecture);
            req.getRequestDispatcher("/lectures_edit.jsp").forward(req, resp);
        } catch (LectureDAOException e) {
            log.error(e);
            req.setAttribute("error", "Cannot get lecture with this ID: " + e.getMessage());
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
            dispatcher.forward(req, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req.getParameter("id") == null)
        {
            req.setAttribute("error", "ID not getted");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
            dispatcher.forward(req, resp);
        }
        int id = Integer.parseInt(req.getParameter("id"));

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
            Date parsedDate = dateFormat.parse(req.getParameter("start"));

            HashMap<String, Object> data = new HashMap<>();
            for (Object key:
                    req.getParameterMap().keySet()) {
                String _key = key.toString();
                String[] value = req.getParameterMap().get(key);
                data.put(_key, value[0]);
            }

            long inId  = lectureService.saveLecture(data);
            if(inId > 0)
            {
                resp.sendRedirect("/lectures/");
            }
            else {
                req.setAttribute("error", "Cannot update lecture with this ID: " + id);
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
                dispatcher.forward(req, resp);
            }
        } catch (ModelException | LectureDAOException | ParseException e) {
            log.error(e.getStackTrace());
            req.setAttribute("error", "Cannot save lecture with this ID: " + e.getMessage());
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
            dispatcher.forward(req, resp);
        }
    }
}
