package students.controllers.listeners;

import students.common.notification.LectionNotificator;
import students.models.pojo.Lecture;
import org.apache.log4j.Logger;
import students.services.LectureServiceImpl;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.List;

public class ApplicationLoadListener implements ServletContextListener {
    private static Logger log = Logger.getLogger(ApplicationLoadListener.class);

    private static Logger logger = Logger.getLogger(ApplicationLoadListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        notifyByNearestLection();

        Thread th1 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(60 * 60 * 1000);
                        notifyByNearestLection();
                    } catch (InterruptedException e) {
                        logger.error(e);
                    }
                }
            }
        });

        th1.start();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        logger.trace("site was stopped");
    }

    public void notifyByNearestLection() {
        List<Lecture> lections = LectureServiceImpl.getNearedLection();
        if (lections.size() > 0) {
            logger.trace("lections founded");
            for (Lecture lection :
                    lections) {
                LectionNotificator.notifyByLection(lection);
            }
        } else {
            logger.trace("neared lections not found");
        }
    }
}
