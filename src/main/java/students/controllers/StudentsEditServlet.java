package students.controllers;

import students.models.dao.StudentDAOImpl;
import students.models.pojo.Student;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class StudentsEditServlet extends HttpServlet {

    private static Logger log = Logger.getLogger(StudentsEditServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req.getParameter("id") == null)
        {
            req.setAttribute("error", "ID not getted");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
            dispatcher.forward(req, resp);
        }

        int id = Integer.parseInt(req.getParameter("id"));
        StudentDAOImpl studentDao = new StudentDAOImpl();
        try (ResultSet rs = studentDao.findFirst(id)){
            Student student = new Student();
            int rows = 0;

            if (rs.last()) {
                rows = rs.getRow();
                rs.beforeFirst();
            }

            if(rows == 0) {
                req.setAttribute("error", "Student not found with this id");
                req.getServletContext().getRequestDispatcher("/error.jsp").forward(req, resp);
            }
            while (rs.next()) {
                student.setId(rs.getInt("id"));
                student.setName(rs.getString("name"));
                student.setGroup_id(rs.getInt("group_id"));
                student.setBirthday(rs.getString("birthday"));
                student.setSex(rs.getString("sex"));
            }
            req.setAttribute("student", student);
            req.getRequestDispatcher("/students_edit.jsp").forward(req, resp);
        } catch (SQLException e) {
            log.error(e);
            req.setAttribute("error", "Cannot get student with this ID: " + e.getMessage());
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
            dispatcher.forward(req, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        StudentDAOImpl studentDAO = new StudentDAOImpl();
        try {

            HashMap<String, Object> data = new HashMap<>();
            data.put("id", req.getParameter("id"));
            data.put("group_id", req.getParameter("group_id"));
            data.put("name", req.getParameter("name"));
            data.put("birthday", req.getParameter("birthday"));
            data.put("sex", req.getParameter("sex"));
            data.put("email", req.getParameter("email"));

            long inId  = studentDAO.save(data, studentDAO);
            if(inId > 0)
            {
                resp.sendRedirect("/students/");
            }
            else {
                req.setAttribute("error", "Cannot update student with this ID: " + id);
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
                dispatcher.forward(req, resp);
            }
        } catch (SQLException e) {
            log.error(e);
            req.setAttribute("error", "Cannot get studetn with this ID: " + e.getMessage());
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
            dispatcher.forward(req, resp);
        }
    }
}
