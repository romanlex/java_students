package students.controllers;

import students.services.LectureServiceImpl;
import students.common.exceptions.LectureDAOException;
import students.exceptions.ModelException;
import students.models.pojo.Lecture;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LecturesDeleteServlet extends HttpServlet {
    private static Logger log = Logger.getLogger(LecturesDeleteServlet.class);

    @Autowired
    private LectureServiceImpl lectureService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
;
        try {
            Lecture lecture = lectureService.getLectureById(id);
            if(lectureService.deleteLecture(lecture) > 0)
                resp.sendRedirect("/lectures/");
            else {
                req.setAttribute("error", "Cannot remove lecture with this ID: " + id);
                req.getRequestDispatcher("/error.jsp").forward(req,resp);
            }
        } catch (ModelException | LectureDAOException e) {
            log.error(e);
            req.setAttribute("error", "Cannot delete lecture with this ID: " + e.getMessage());
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
            dispatcher.forward(req, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
