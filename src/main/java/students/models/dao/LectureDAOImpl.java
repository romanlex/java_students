package students.models.dao;

import students.common.exceptions.LectureDAOException;
import students.models.Model;
import students.models.connector.Database;
import students.models.pojo.Lecture;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class LectureDAOImpl extends Model implements LectureDAO {
    private static final String table = "lection";
    private static Logger log = Logger.getLogger(LectureDAOImpl.class);
    private static String SQL_NEARED_LECTIONS = "SELECT * FROM lection WHERE date >? AND date <?";

    public List<Lecture> getNearedLections() throws LectureDAOException {
        List<Lecture> lections = new ArrayList<>();
        Connection connection = Database.INSTANCE.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_NEARED_LECTIONS);
            preparedStatement.setTimestamp(1,new Timestamp(System.currentTimeMillis()));
            preparedStatement.setTimestamp(2,new Timestamp(System.currentTimeMillis()+60*60*1000));
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
                log.debug(resultSet.getString("name"));

                Lecture lection = new Lecture(resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("subject"),
                        resultSet.getString("textLection"),
                        resultSet.getInt("groupid"),
                        resultSet.getDate("date"));
                lections.add(lection);
            }
        } catch (SQLException e) {
            log.error(e.getStackTrace());
            throw new LectureDAOException(e.getMessage());
        }

        log.debug(lections.size());
        return lections;
    }


}
