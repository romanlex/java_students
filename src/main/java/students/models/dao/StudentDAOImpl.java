package students.models.dao;

import students.models.Model;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value="session")
public class StudentDAOImpl extends Model implements StudentDAO {
    private static final String table = "students";
    private static Logger log = Logger.getLogger(StudentDAOImpl.class);

}
