package students.models.dao;

import students.common.exceptions.LectureDAOException;
import students.models.pojo.Lecture;

import java.util.List;

public interface LectureDAO {
    List<Lecture> getNearedLections() throws LectureDAOException;
}
