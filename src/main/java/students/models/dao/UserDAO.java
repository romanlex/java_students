package students.models.dao;

import students.common.exceptions.UserDAOException;
import students.models.Model;
import students.models.connector.Database;
import students.models.pojo.User;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
@Scope(value="session")
public class UserDAO extends Model {
    private static final String table = "students";
    private static Logger log = Logger.getLogger(UserDAO.class);
    private static String FIND_USER_SQL = "SELECT * FROM users WHERE login=? AND password=?";

    public User getUserByLoginAndPassword(String login, String password) throws UserDAOException {
        User user = new User();
        Connection db = Database.INSTANCE.getConnection();
        try (PreparedStatement ps = db.prepareStatement(FIND_USER_SQL)) {
            ps.setString(1, login);
            ps.setString(2, password);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                user.setId(result.getInt("id"));
                user.setLogin(result.getString("login"));
                user.setPassword(result.getString("password"));
                user.setRole(result.getString("role"));
            }
        } catch (SQLException e) {
            log.error(e);
            throw new UserDAOException();
        }
        return user;
    }
}
