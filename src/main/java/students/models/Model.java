package students.models;

import students.exceptions.ModelException;
import students.models.connector.Database;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class Model {
    public String tablename;
    private static final Logger log = Logger.getLogger(Model.class);

    /**
     * Save object to database
     * @param map map with key value pair
     * @return long LAST_INSERT_ID
     * @throws SQLException Error in sql
     */
    public long save(HashMap<String, Object> map, Model clazz) throws ModelException {
        Connection db = Database.INSTANCE.getConnection();
        boolean update = false;
        ResultSet generatedKeys;
        long insertedId = -1;
        int rowExistedId = 0;

        try {
            if (map.get("id") != null) {
                rowExistedId = Integer.parseInt(map.get("id").toString());
                ResultSet rs = clazz.findFirst(rowExistedId);
                int rows = 0;

                if (rs.last()) {
                    rows = rs.getRow();
                    rs.beforeFirst();
                }

                if (rows > 0) {
                    update = true;
                }
            }

            List<String> keys = new ArrayList<>(map.keySet());
            List<String> questions = new ArrayList<>(map.keySet());
            questions.replaceAll(s -> s != null ? "?" : "");
            String fields = String.join(", ", keys);
            String _values = String.join(", ", questions);
            String query;
            if (!update)
                query = "INSERT INTO " + tablename + " (" + fields + ") VALUES (" + _values + ");";
            else {
                keys.remove("id");
                map.remove("id");
                List<String> sql = new ArrayList<>();
                for (String key :
                        keys) {
                    sql.add(key + "=?");
                }
                String _fieldvalues = String.join(", ", sql);
                query = "UPDATE " + tablename + " SET  " + _fieldvalues + " WHERE id = " + rowExistedId + ";";
            }
            PreparedStatement preparedStatement = db.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            int i = 1;
            for (String key :
                    keys) {

                Object value = (map.get(key) != null ? map.get(key) : "");
                String type = value.getClass().getSimpleName();
                switch (type) {
                    case "String":
                        preparedStatement.setString(i, value.toString());
                        break;
                    case "Double":
                        preparedStatement.setDouble(i, Double.parseDouble(value.toString()));
                        break;
                    case "Float":
                        preparedStatement.setFloat(i, Float.parseFloat(value.toString()));
                        break;
                    case "Integer":
                        preparedStatement.setInt(i, Integer.parseInt(value.toString()));
                        break;
                    case "Long":
                        preparedStatement.setLong(i, Long.parseLong(value.toString()));
                        break;
                    case "Date":
                        preparedStatement.setDate(i, java.sql.Date.valueOf(value.toString()));
                        break;
                    case "Timestamp":
                        preparedStatement.setTimestamp(i, Timestamp.valueOf(value.toString()));
                        break;
                }
                i++;
            }

            log.debug(preparedStatement.toString());

            int affectedRows = preparedStatement.executeUpdate();

            if (update)
                return affectedRows;

            if (affectedRows == 0) {
                throw new SQLException("Creating " + this.getClass() + " failed, no rows affected.");
            }

            generatedKeys = preparedStatement.getGeneratedKeys();

            if (generatedKeys.next()) {
                insertedId = generatedKeys.getLong(1);
            } else
                throw new SQLException("Creating " + this.getClass() + " failed, no ID obtained.");
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new ModelException(e.getMessage());
        }

        return insertedId;
    }


    public ResultSet find() throws ModelException {
        ResultSet rs;
        Connection db = Database.INSTANCE.getConnection();
        try {
            Statement query = db.createStatement();
            rs = query.executeQuery("SELECT * FROM " + tablename);
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new ModelException(e.getMessage());
        }
        return rs;
    }


    public ResultSet findFirst(int id) throws ModelException {
        ResultSet rs;
        Connection db = Database.INSTANCE.getConnection();
        try {
            Statement query = db.createStatement();
            rs = query.executeQuery("SELECT * FROM " + tablename + " WHERE id = " + id);
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new ModelException(e.getMessage());
        }
        return rs;
    }

    public int deleteFirst(int id) throws ModelException {
        int result = 0;
        Connection db = Database.INSTANCE.getConnection();
        try {
            Statement query = db.createStatement();
            String sql = "DELETE FROM " + tablename + " WHERE id = ?";
            PreparedStatement ps = db.prepareStatement(sql);
            ps.setInt(1, id);
            result = ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new ModelException(e.getMessage());
        }
        return result;
    }

}
