package students.models.connector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    public static final Database INSTANCE = new Database();
    private static Connection db;

    private static final String dbname = "java_students";
    private static final String user = "root";
    private static final String password = "dbadmin2015";

    public Database() {

    }

    private Connection newConnection() {
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            System.out.println("Driver loading succes!");
            db = DriverManager.getConnection("jdbc:mariadb://localhost:3306/" + dbname + "?user="+user+"&password="+password);
            return db;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Connection getConnection() {
        if(db == null)
            return newConnection();
        else
            return db;
    }
}
