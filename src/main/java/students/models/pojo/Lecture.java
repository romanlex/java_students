package students.models.pojo;

import java.util.Date;

public class Lecture {
    private int id;
    private String title;
    private String text;
    private String subject;
    private int groupid;
    private Date start;

    public Lecture() {
    }

    public Lecture(int id, String title, String text, String subject, int groupid, Date start) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.subject = subject;
        this.groupid = groupid;
        this.start = start;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }
}
