package students.models.pojo;

public class Student {
    private int id;
    private int group_id;
    private String name;
    private String birthday;
    private String sex;

    public Student() {

    }

    public Student(int id, int group_id, String name, String birthday, String sex) {
        this.id = id;
        this.group_id = group_id;
        this.name = name;
        this.birthday = birthday;
        this.sex = sex;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
