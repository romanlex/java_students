package students.services;

import students.common.exceptions.UserDAOException;
import students.models.dao.UserDAO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
//@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserServiceImpl implements UserService  {
    private static Logger log = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDAO userDAO;
    private int anInt = 0;
    private static int authCnt = 0;


    @Autowired
    public UserServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public boolean authenticated(String login, String password) throws UserDAOException {
        if(anInt == 0) {
            Random rnd = new Random();
            anInt = rnd.nextInt();
        }
        log.trace("anInt: " + anInt);

        authCnt++;
        log.trace("Authentification count: " + authCnt);

        if(userDAO.getUserByLoginAndPassword(login,password).getId() != 0) {
            return true;
        } else {
            return false;
        }
    }
}
