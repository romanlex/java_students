package students.services;

import students.common.exceptions.UserDAOException;

public interface UserService {
    boolean authenticated(String login, String password) throws UserDAOException;
}
