package students.services;

import students.common.exceptions.LectureDAOException;
import students.exceptions.ModelException;
import students.models.dao.LectureDAOImpl;
import students.models.pojo.Lecture;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class LectureServiceImpl implements LectureService {
    private static Logger log = Logger.getLogger(LectureServiceImpl.class);

    @Autowired
    private LectureDAOImpl lectureDAO;

    public long saveLecture(HashMap<String, Object> data) throws LectureDAOException, ModelException {
        return lectureDAO.save(data, lectureDAO);
    }

    public Lecture getLectureById(int id) throws LectureDAOException {
        Lecture lecture = new Lecture();
        try {
            ResultSet rs = lectureDAO.findFirst(id);
            int rows = 0;

            if (rs.last()) {
                rows = rs.getRow();
                rs.beforeFirst();
            }
            if(rows == 0) {
                throw new LectureDAOException("Lecture not found with this id");
            }

            while (rs.next()) {
                lecture.setId(rs.getInt("id"));
                lecture.setStart(rs.getTimestamp("start"));
                lecture.setSubject(rs.getString("subject"));
                lecture.setText(rs.getString("text"));
                lecture.setTitle(rs.getString("title"));
            }

        } catch (ModelException | SQLException e) {
            log.error(e.getStackTrace());
            throw new LectureDAOException(e.getMessage());
        }
        return lecture;
    }

    public int deleteLecture(Lecture lecture) throws LectureDAOException, ModelException {
        return lectureDAO.deleteFirst(lecture.getId());
    }

    public List<Lecture> getNearedLection() throws LectureDAOException {
        return lectureDAO.getNearedLections();
    }

}
