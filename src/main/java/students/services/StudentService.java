package students.services;

import students.common.exceptions.StudentDAOException;
import students.exceptions.ModelException;
import students.models.pojo.Student;

import java.util.HashMap;

public interface StudentService {
    boolean addStudent(HashMap<String, Object> data) throws ModelException, StudentDAOException;
    int deleteStudent(Student student) throws ModelException;
}
