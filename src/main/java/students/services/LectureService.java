package students.services;

import students.common.exceptions.LectureDAOException;
import students.exceptions.ModelException;
import students.models.pojo.Lecture;

import java.util.HashMap;

public interface LectureService {
    long saveLecture(HashMap<String, Object> data) throws LectureDAOException, ModelException;
    int deleteLecture(Lecture lecture) throws LectureDAOException, ModelException;
}
