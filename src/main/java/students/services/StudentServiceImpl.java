package students.services;

import students.common.exceptions.LectureDAOException;
import students.common.exceptions.StudentDAOException;
import students.exceptions.ModelException;
import students.models.dao.StudentDAOImpl;
import students.models.pojo.Student;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

@Service
public class StudentServiceImpl implements StudentService {
    private static Logger log = Logger.getLogger(StudentServiceImpl.class);

    @Autowired
    private StudentDAOImpl studentDAO;

    @Override
    public boolean addStudent(HashMap<String, Object> data) throws StudentDAOException, ModelException {
        return studentDAO.save(data, studentDAO) > 0;
    }

    public int deleteStudent(Student student) throws ModelException {
        return studentDAO.deleteFirst(student.getId());
    }

    public Student getStudentById(int id) throws LectureDAOException {
        Student student = new Student();
        try {
            ResultSet rs = studentDAO.findFirst(id);
            int rows = 0;

            if (rs.last()) {
                rows = rs.getRow();
                rs.beforeFirst();
            }
            if(rows == 0) {
                throw new LectureDAOException("Student not found with this id");
            }

            while (rs.next()) {
                student.setId(rs.getInt("id"));
                student.setGroup_id(rs.getInt("group_id"));
                student.setBirthday(rs.getString("birthday"));
                student.setName(rs.getString("name"));
                student.setSex(rs.getString("sex"));
            }

        } catch (ModelException | SQLException e) {
            log.error(e.getStackTrace());
            throw new LectureDAOException(e.getMessage());
        }
        return student;
    }
}
