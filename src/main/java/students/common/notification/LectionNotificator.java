package students.common.notification;

import students.services.StudentServiceImpl;
import students.models.pojo.Lecture;
import students.models.pojo.Student;
import org.apache.log4j.Logger;

import java.util.List;

public class LectionNotificator {
    private static Logger logger = Logger.getLogger(LectionNotificator.class);

    public static void notifyByLection(Lecture lection){
        List<Student> students = StudentServiceImpl.getStudentsByGroupId(lection.getGroupid());
        logger.trace(students.size() + "founded");
        for (Student student:
                students) {
            Mailer.sendMail(student.getEmail(),"lection is neared", lection.getSubject());
        }
    }
}
