package students.common.exceptions;

public class LectureDAOException extends Exception {
    public LectureDAOException(String s) {
        super(s);
    }
}
