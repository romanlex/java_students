<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1, user-scalable=yes">

    <title>Login</title>
</head>
<body>

<form action="" method="post">
    <div class="form-group">
        <label for="login">Логин:</label>
        <input type="text" name="login" id="login">
    </div>
    <div class="form-group">
        <label for="login">Пароль:</label>
        <input type="password" name="password">
    </div>
    <div class="form-buttons">
        <button type="submit">Авторизоваться</button>
    </div>
</form>
<a href="/join">Регистрация</a>

</body>
</html>
