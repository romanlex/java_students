<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1, user-scalable=yes">

    <title>Добавить студента</title>
</head>
<body>
<style>
    label {
        cursor: pointer;
    }
    .form-group {
        margin-bottom: 15px;
    }
    .form-group label, .gender > span {
        display: inline-block;
        padding-right: 15px;
        width: 200px;
        text-align: right;
    }
</style>
<a href="/dashboard">На главную</a><br><br>
<a href="/students/list">Назад к списку студентов</a>
<br>
<br>
<form action="/students/add" method="post">
    <div class="form-group">
        <label for="group_id">Номер группы:</label>
        <input type="text" name="group_id" id="group_id">
    </div>
    <div class="form-group">
        <label for="name">Имя:</label>
        <input type="text" name="name" id="name">
    </div>
    <div class="form-group">
        <label for="birthday">День рождения:</label>
        <input type="text" name="birthday" id="birthday">
    </div>
    <div class="gender">
        <span>Пол:</span>

        <input type="radio" id="man" value="f" name="sex" checked>
        <label for="man">Мужской</label>

        <input type="radio" id="woman" value="m" name="sex">
        <label for="woman">Женский</label>
    </div>
    <hr>
    <div class="form-buttons">
        <button type="submit">Добавить</button>
    </div>
</form>
</body>
</html>
