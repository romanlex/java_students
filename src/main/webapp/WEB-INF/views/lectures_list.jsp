<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="students.models.pojo.Lecture" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1, user-scalable=yes">

    <title>Students</title>
</head>
<body>
<div class="wrapper">
    <div class="content">
        <a href="/dashboard">На главную</a><br><br>
        <h1>Lectures list from database</h1>
        <br>
        <table border="1" style="min-width: 60%">
            <thead style="font-weight: bold;">
                <tr>
                    <td>#id</td>
                    <td>title</td>
                    <td>text</td>
                    <td>subject</td>
                    <td>start</td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
            <c:if test="${not empty lectures}">
                <c:forEach items="${lectures}" var="lecture">
                    <tr>
                        <td>
                            <c:if test="${not empty lecture.id}">
                                <c:out value="${lecture.id}"></c:out>
                            </c:if>
                            <c:if test="${empty lecture.id}">Не указано</c:if>
                        </td>
                        <td>
                            <c:if test="${not empty lecture.title}">
                                <c:out value="${lecture.title}"></c:out>
                            </c:if>
                            <c:if test="${empty lecture.title}">Не указано</c:if>
                        </td>
                        <td>
                            <c:if test="${not empty lecture.text}">
                                <c:out value="${lecture.text}"></c:out>
                            </c:if>
                            <c:if test="${empty lecture.text}">Не указано</c:if>
                        </td>
                        <td>
                            <c:if test="${not empty lecture.subject}">
                                <c:out value="${lecture.subject}"></c:out>
                            </c:if>
                            <c:if test="${empty lecture.subject}">Не указано</c:if>
                        </td>
                        <td>
                            <c:if test="${not empty lecture.start}">
                                <c:out value="${lecture.start}"></c:out>
                            </c:if>
                            <c:if test="${empty lecture.start}">Не указано</c:if>
                        </td>
                        <td>
                            <a href="/lectures/edit?id=${lecture.id}">Редактировать</a>
                            <a href="/lectures/delete?id=${lecture.id}">Удалить</a>
                        </td>
                    </tr>
                </c:forEach>
            </c:if>
            <c:if test="${empty lectures}">
                <tr>
                    <td colspan="5">
                        Студентов не найдено
                    </td>
                </tr>
            </c:if>
            </tbody>
        </table>
        <a href="/lectures/add">Добавить лекцию</a>
    </div>
</div>
</body>
</html>
