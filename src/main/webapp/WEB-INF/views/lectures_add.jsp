<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1, user-scalable=yes">

    <title>Добавить лекцию</title>
</head>
<body>
<style>
    label {
        cursor: pointer;
    }
    .form-group {
        margin-bottom: 15px;
    }
    .form-group label, .gender > span {
        display: inline-block;
        padding-right: 15px;
        width: 200px;
        text-align: right;
    }
</style>
<a href="/dashboard">На главную</a><br><br>
<a href="/lectures/list">Назад к списку</a>
<br>
<br>
<form action="/lectures/add" method="post">
    <div class="form-group">
        <label for="title">Наименование лекции:</label>
        <input type="text" name="title" id="title">
    </div>
    <div class="form-group">
        <label for="text">Текст лекции:</label>
        <textarea name="text" id="text"></textarea>
    </div>
    <div class="form-group">
        <label for="subject">Тема лекции:</label>
        <input type="text" name="subject" id="subject">
    </div>
    <div class="form-group">
        <label for="start">Начало лекции:</label>
        <input type="datetime-local" name="start" id="start">
    </div>
    <hr>
    <div class="form-buttons">
        <button type="submit">Добавить</button>
    </div>
</form>
</body>
</html>
