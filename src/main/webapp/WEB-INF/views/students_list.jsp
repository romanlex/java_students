<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="students.models.pojo.Student" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1, user-scalable=yes">

    <title>Students</title>
</head>
<body>
<div class="wrapper">
    <div class="content">
        <a href="/dashboard">На главную</a><br><br>
        <h1>Students list from database</h1>
        <br>
        <table border="1" style="min-width: 60%">
            <thead style="font-weight: bold;">
                <tr>
                    <td>#id</td>
                    <td>Group ID</td>
                    <td>Name</td>
                    <td>birthday</td>
                    <td>Gender</td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
            <c:if test="${not empty students}">
                <c:forEach items="${students}" var="student">
                    <tr>
                        <td>
                            <c:if test="${not empty student.id}">
                                <c:out value="${student.id}"></c:out>
                            </c:if>
                            <c:if test="${empty student.id}">Не указано</c:if>
                        </td>
                        <td>
                            <c:if test="${not empty student.group_id}">
                                <c:out value="${student.group_id}"></c:out>
                            </c:if>
                            <c:if test="${empty student.group_id}">Не указано</c:if>
                        </td>
                        <td>
                            <c:if test="${not empty student.name}">
                                <c:out value="${student.name}"></c:out>
                            </c:if>
                            <c:if test="${empty student.name}">Не указано</c:if>
                        </td>
                        <td>
                            <c:if test="${not empty student.birthday}">
                                <c:out value="${student.birthday}"></c:out>
                            </c:if>
                            <c:if test="${empty student.birthday}">Не указано</c:if>
                        </td>
                        <td>
                            <c:if test="${not empty student.sex}">
                                <c:out value="${student.sex}"></c:out>
                            </c:if>
                            <c:if test="${empty student.sex}">Не указано</c:if>
                        </td>
                        <td>
                            <a href="/students/edit?id=${student.id}">Редактировать</a>
                            <a href="/students/delete?id=${student.id}">Удалить</a>
                        </td>
                    </tr>
                </c:forEach>
            </c:if>
            <c:if test="${empty students}">
                <tr>
                    <td colspan="5">
                        Студентов не найдено
                    </td>
                </tr>
            </c:if>
            </tbody>
        </table>
        <a href="/students/add">Добавить студента</a>
    </div>
</div>
</body>
</html>
