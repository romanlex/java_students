<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1, user-scalable=yes">

    <title>Добавить студента</title>
</head>
<body>
<style>
    label {
        cursor: pointer;
    }
    .form-group {
        margin-bottom: 15px;
    }
    .form-group label, .gender > span {
        display: inline-block;
        padding-right: 15px;
        width: 200px;
        text-align: right;
    }
</style>

<a href="/lectures/">Лекции</a>
<a href="/students/">Студенты</a>

</body>
</html>
